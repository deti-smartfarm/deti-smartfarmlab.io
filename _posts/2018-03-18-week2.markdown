---
layout: post
title:  "Week 2!"
date:   2018-03-18 19:36:23
permalink: /week2.html
---
<span class="image featured"><img src="{{ site.baseurl }}/images/logo-medium.png" alt=""></span>

Progress has been made! **SmartFarm** is taking shape as we get our hands dirty
and start coding.

We started by emulating the sensors by using a Python script that randomly sends
messages to the broker, the latter in which already flows some data, which is
passed via Web Sockets to the front-end.

Our front-end is currently a dashboard built on React and it is
only showing the values the broker sends by the moment.

Our website is already up and running with an initial draft of the documentation
we intent to provide.

Week 3, here we go.
