---
layout: post
title:  "Week 13!"
date:   2018-06-03 15:00:00
permalink: /week13.html
---
<span class="image featured"><img src="{{ site.baseurl }}/images/logo-medium.png" alt=""></span>

On this last week we presented the M6, on which the main target was to present the main scenarios.

We have also done the final configurations for the presentation and delivery, along with revision of documentation. We had some problems due to the availability of Jenkins inside the UA network and the disk space, which was not much. Finally, we have done changes to our monitoring logs, to ease the search of our micro-services logs to allow an easier debugging.