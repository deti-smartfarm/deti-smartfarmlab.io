---
layout: post
title:  "Week 8!"
date:   2018-04-29 15:00:00
permalink: /week8.html
---
<span class="image featured"><img src="{{ site.baseurl }}/images/logo-medium.png" alt=""></span>

On this week, we started by integrating the Kafka Streams module to calculate the average values sent by every sensor. Altough it is still not fully done, it is expected to be done on the next week

On the UI, the sensor page now displays a map with the localization of the sensors, and the chart with the values sent from each sensor was fixed. It is possible to do the farm management directly on the UI, as well as see the triggered alerts.

It was also added a reverse proxy, the Nginx, to provide an uniform URL for the external services.