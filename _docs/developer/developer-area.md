---
layout: documentation-default
category: Developer
order: 1
---

## Developer Area

This area describes all the relevant development details which are useful to
understand and continue the progressive work around this platform.
The development of all modules was made following the microservices principles,
being intended to be scalable and independent between modules.

Since our API is bit long, we provide an external document with it
[here]({{ site.baseurl }}/assets/api-definition.pdf).