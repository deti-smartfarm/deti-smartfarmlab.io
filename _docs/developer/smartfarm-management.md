---
layout: documentation-default
category: Developer
order: 4
---

## SmartFarm Management (Service Layer)

The **SmartFarm Management** module (or also called Service Layer)
includes particular components that define and manage
Farms, Sensors and Users. Each one provides HTTP REST endpoints that support
CRUD operations to manage its data and it is protected by the developed
Authentication System, which is later described. This module runs on the
*SpringBoot* framework, simplifying the implementation and any changes that
need to be done.

It also provides an Websocket endpoint for actuators (which are, in reality,
also sensors).

An SQL database supports all of the data model (*PostgreSQL*), but it can be easily
changed to different one. It's import to refer that all address pointing to
the databases used on the different modules are accessed by the Java modules
via environment variables.

This module contains a variety of files:

* `Farms.java`: defines the Farms JPA entity;
* `Sfusers.java`: defines the Users JPA entity;
* `SfuserController.java`: although a User CRUD Repository is also created, this
controller lets the users register and login in the platform since it provides
non-authenticated endpoints;
* `SfusersDetails.java`: user data used by the authenticated system to actually
authenticate the user;
* `Sensors.java`: defines the Sensors JPA entity;
* `*Repository.java`: creates a CRUD repository, i.e., in this particular,
makes a HTTP API available with CRUD methods to interact with the various
data entities;
* `SendDataToSensor.java`: creates an endpoint to send data to an actuator (sensor);
* `WebSocketConfig.java`: configuration of the WebSocket;


## Authentication System

In order to protect the farmers account and platform overall, SmartFarm access
is based on *Springboot's Open Authorization (OAuth2)* protocol implementation.

It is noteworthy the fact that, although all the main endpoints relative to
User, Farm and Sensors management is protected and the client needs to be
authenticated in order to use any of the provided endpoints, the use of any of
the other provided endpoints in the SmartFarm API is **not** authenticated and,
consequently, not totally secure. The choice to have only the management system
authenticated was due to the easily integration of the User Management endpoints
to manage user authentications and to development time constraints.
On the other hand, since the authentication uses OAuth2, it can be easily
integrated with the other endpoints (and respective modules).

Following the previous paragrah, it is also import to note that, although not
having all the endpoints authenticated, the developed Web Portal only lets
authenticated users browse through the platform.

Wrapping up, the authentication system uses two main endpoints:

* `authorize`: request a temporary token using the clients credentials. If the
user is registered in the platform, a succesful response will arrive with the
token to use with the endpoints that request authentication.

* `check_token`: check if a given token is valid (mainly used within the Web
Portal to check if a user is logged in/authenticated).

Although the previous endpoints will be further detailed in the API section of
this category as well as the endpoints that request authentication, we will also
mention that, in order to consume any of those authenticated endpoints, an HTTP
`Authentication` header needs to be in the request, using as its value
`Bearer user_token`.

The Authentication System is implemented in the same module as the User, Farm
and Sensors Management, i.e., the SmartFarm Service Layer.
The following files implement the overall system:

* `AuthorizationServerConfig`: configures the Authentication and Authorization
server using the Springboot's security framework;
* `ResourceServerConfig`: configures the current application (in this case the
Service Layer) as a resource server and which endpoints are publicly open and
which are closed and request authentication.