---
layout: documentation-default
category: Team
order: 2
---

## Milestones

The following milestones will help us to fulfill the objectives through the
development of the platform.

### M1 - March 19th

* Sensors' data flowing through broker;
* Basic UI working and display values from a sensor at real-time.


### M2 - April 14th

* Data coming from sensors should be saved on a database;
* UI should correctly display values coming from sensors and the historic data.

### M3 - April 28th

* UI should allow device management, it should be possible to send actions
to the actuators and an historic view of data should be presented.
* Data processing (averages);
* Reverse proxy and load balancing.

### M4 - May 12th

* Basic users management (AAA) should be done;
* Alarming module should be finished and all features should be implemented;
* Basic acceptance testing and system logging.

### M5 - May 26th
* Final touches on the UI;
* Final debugging on the platform and unit & integration testing.


### M6 - June 4th

* Final deployment;
* Final presentation.

### M7 - June 10th

* Final delivery.