---
layout: documentation-default
category: Team
order: 1
---

## The Team

We are an ambicious team and we're working to make **SmartFarm**
a great platform!

<section class="wrapper style1 special">
<div class="row">
                            <div class="4u">

                                <section>
                                    <img class="team-img" style="max-width:55%;" src="https://avatars2.githubusercontent.com/u/9132222?s=400&u=1f4897c27a0382272c206d3271e0115a945c8f35&v=4"/>
                                    <header>
                                        <h3>Daniela Simões</h3>
                                    </header>
                                    <p class="team-mail">
                                        danielasimoes@ua.pt
                                        <br />76771
                                    </p>
                                </section>

                            </div>


                            <div class="4u">

                                <section>
                                    <img class="team-img" style="max-width:55%;" src="https://avatars2.githubusercontent.com/u/11805521?v=3&s=460.jpg"/>
                                    <header>
                                        <h3>Diogo Ferreira</h3>
                                    </header>
                                    <p class="team-mail">
                                        pdiogoferreira@ua.pt
                                        <br /> 76425
                                    </p>
                                </section>

                            </div>
                            <div class="4u">

                                <section>
                                    <img class="team-img" style="max-width:55%;" src="https://avatars3.githubusercontent.com/u/10684106?.jpg"/>
                                    <header>
                                        <h3>Diogo Ferreira</h3>
                                    </header>
                                    <p class="team-mail">
                                        diogodanielsoaresferreira@ua.pt
                                        <br />76504
                                    </p>
                                </section>

                            </div>
                        </div>


                        <div class="row">
                            <div class="6u">

                                <section>
                                    <img class="team-img" style="max-width:40%;" src="https://avatars3.githubusercontent.com/u/37042888?s=400&v=4.jpg"/>
                                    <header>
                                        <h3>Luís Leira</h3>
                                    </header>
                                    <p class="team-mail">
                                        luisleira@ua.pt
                                        <br />76514
                                    </p>
                                </section>

                            </div>
                            <div class="6u">

                                <section>
                                    <img class="team-img" style="max-width:40%;" src="https://avatars2.githubusercontent.com/u/10819202?v=3&s=460.jpg"/>
                                    <header>
                                        <h3>Pedro Martins</h3>
                                    </header>
                                    <p class="team-mail">
                                        pbmartins@ua.pt
                                        <br />76551
                                    </p>
                                </section>

                            </div>
                        </div>
                        </section>